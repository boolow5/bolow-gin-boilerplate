package db

import (
	"fmt"
	"strings"
)

// IsNotFound takes error and checks if it contains not found
func IsNotFound(err error) bool {
	if err == nil {
		return false
	}
	return strings.Contains(err.Error(), "not found")
}

// IsDuplicationError takes error and checks if it contains duplicate key error
func IsDuplicationError(err error) bool {
	fmt.Printf("IsDuplicationError(err: %s)\n", err.Error())
	if err == nil {
		return false
	}
	return strings.Contains(err.Error(), "duplicate key error")
}
