package controllers

import (
	"fmt"
	"strings"

	"github.com/gin-gonic/gin"
)

const (
	// UknownError is error which was not categorized under known errors
	UknownError = iota
	// MissingFieldError indicates posted data is missing required field
	MissingFieldError
	// BadFormDataError indicates posted form failed to bind with target object
	BadFormDataError
	// SavingItemError indicates an item could not be saved to database
	SavingItemError
	// AlreadyExists indicates fields were already used to register
	AlreadyExists
	// NotFoundError indicates item does not exist in the system
	NotFoundError
	// TokenGenerationError indicates JWT token generation failed
	TokenGenerationError
	// AuthenticationError indicates login failed
	AuthenticationError
	// DataFetchingError indicates fetching data was not successful
	DataFetchingError
	// PermissionError indicates current user does not have user level needed
	PermissionError
	// TransactionError indicates error is related to transfer of funds between wallets
	TransactionError
)

// ErrorMessages string version of the above error constants
var ErrorMessages = []string{
	"unkown error",
	"required field is missing",
	"failed to accept posted data",
	"failed to save item",
	"already used",
	"item not found",
	"token generation failed",
	"authentication failed",
	"fetching data failed",
	"permission error",
	"transaction error",
}

// RequestError is custom error for user requests
type RequestError struct {
	Code     int    `json:"code" bson:"code"`
	Message  string `json:"msg" bson:"msg"`
	MoreInfo string `json:"more" bson:"more"`
}

// Error satisfies error interface
func (me RequestError) Error() string {
	return fmt.Sprintf("Error %d: %s", me.Code, me.Message)
}

// NewError creates RequestError
func NewError(code int, moreInfo string) RequestError {
	if len(ErrorMessages) > code {
		return RequestError{Code: code, Message: ErrorMessages[code], MoreInfo: moreInfo}
	}
	return RequestError{Code: code, Message: "Unknown error", MoreInfo: moreInfo}
}

// NewErrors creates slice of RequestError
func NewErrors(code int, errMessages []error) []RequestError {
	errs := []RequestError{}
	for _, err := range errMessages {
		if err != nil {
			errs = append(errs, []RequestError{{Code: code, Message: err.Error()}}...)
		}
	}
	return errs
}

// AbortWithError is shortcut for aborting http request with error message and error code
func AbortWithError(c *gin.Context, status int, err RequestError, warnings ...string) {
	msg := struct {
		Msg      string   `json:"msg"`
		Reason   string   `json:"reason"`
		Warnings []string `json:"warnings"`
	}{
		Msg:      err.Error(),
		Reason:   err.MoreInfo,
		Warnings: warnings,
	}
	c.JSON(status, msg)
	c.AbortWithError(status, err)
}

// AbortWithErrors is shortcut for aborting http request with a list of error messages
func AbortWithErrors(c *gin.Context, status int, errs []RequestError, warnings ...string) {
	response := struct {
		Messages []string `json:"messages"`
		Warnings []string `json:"warnings"`
	}{
		Warnings: warnings,
	}
	for _, err := range errs {
		response.Messages = append(response.Messages, []string{err.Error()}...)
	}
	messages := strings.Join(response.Messages, ", ")
	c.JSON(status, response)
	c.AbortWithError(status, fmt.Errorf(messages))
}
