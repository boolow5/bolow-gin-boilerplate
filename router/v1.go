package router

import (
	"net/http"

	"bitbucket.org/boolow5/bolow-gin-boilerplate/controllers"
	"bitbucket.org/boolow5/bolow-gin-boilerplate/middlewares"
	"bitbucket.org/boolow5/bolow-gin-boilerplate/models"
	"github.com/gin-gonic/gin"
)

func setupV1(router *gin.Engine) {

	// routes for everyone, even non-logged in
	v1 := router.Group("/api/v1")
	{
		v1.GET("/version", func(c *gin.Context) {
			c.JSON(http.StatusOK, gin.H{
				"version": models.Version,
				"type":    "v1",
				"build":   models.CommitNo,
				"date":    models.LastBuidDate,
				"domain":  c.Request.Host,
			})
		})
		v1.GET("/ping", controllers.Ping)
		v1.POST("/signup", controllers.Signup)
		v1.POST("/login", controllers.Login)
	}
	authorized := v1.Group("/")

	// routes for all logged in users
	authorized.Use(middlewares.Auth())
	{
		authorized.GET("/profile", controllers.GetProfile)
		authorized.GET("/user-level", controllers.GetUserLevel)
	}
}
