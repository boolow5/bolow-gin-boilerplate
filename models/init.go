package models

import (
	"bitbucket.org/boolow5/bolow-gin-boilerplate/logger"
)

// Init initializes models
func Init() error {
	return nil
}

func createIndexes() {
	logger.Debug("*************** Adding indexes ***************")
	// err := db.AddUniqueIndex(UserCollection, "email", "phone")
	// if err != nil {
	// 	logger.Errorf("Failed to add unique fields 'email,phone' to '%s' collection. Error: %v", UserCollection, err)
	// }
	// err = db.Add2DIndex(DistrictCollection, "$2d:center")
	// if err != nil {
	// 	logger.Errorf("Failed to add 2d index 'center' to '%s' collection. Error: %v", DistrictCollection, err)
	// }
}
