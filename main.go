package main

import (
	"fmt"
	"os"

	"bitbucket.org/boolow5/bolow-gin-boilerplate/config"
	"bitbucket.org/boolow5/bolow-gin-boilerplate/db"
	"bitbucket.org/boolow5/bolow-gin-boilerplate/logger"
	"bitbucket.org/boolow5/bolow-gin-boilerplate/models"
	"bitbucket.org/boolow5/bolow-gin-boilerplate/notifications"
	"bitbucket.org/boolow5/bolow-gin-boilerplate/router"
)

func main() {
	logger.Init(logger.DebugLevel)
	models.Version = config.Get().Version
	fmt.Printf("Starting %s carpooling api...\n", os.Args[0])
	db.Init()
	err := models.Init()
	if err != nil {
		panic(err)
	}
	router := router.Init()
	conf := config.Get()
	go notifications.Start(conf.SSEPort)
	notifications.InitMailGun(conf.MailGun)
	router.Run(conf.Port)
}
