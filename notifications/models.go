package notifications

import (
	"gopkg.in/mgo.v2/bson"
)

// LocationInterface is used to prevent import cycle
type LocationInterface interface {
	Geocode() error
	ReverseGeocode() error
}

// TripMessage represents a trip message sent to drivers
type TripMessage struct {
	TripID          bson.ObjectId     `json:"trip_id"`
	StartLocation   LocationInterface `json:"start_location"`
	Distance        float64           `json:"distance"`
	Price           float64           `json:"price"`
	CurrencySymbol  string            `json:"currency_symbol"`
	VehicleType     string            `json:"vehicle_type"`
	ResponseChannel string            `json:"response_channel"`
}

// DriverFoundMessage represents the message sent to passenger when driver accepts their trip
type DriverFoundMessage struct {
	TripID          bson.ObjectId `json:"trip_id"`
	VehicleID       bson.ObjectId `json:"vehicle_id"`
	ResponseChannel string        `json:"response_channel"`
}
